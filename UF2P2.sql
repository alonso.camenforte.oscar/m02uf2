DROP DATABASE IF EXISTS Pizzeria;
CREATE DATABASE Pizzeria;
USE Pizzeria;

CREATE TABLE client (
    id_client SMALLINT AUTO_INCREMENT,
    nom VARCHAR(20) NOT NULL,
    telefon VARCHAR(9),
    adreca VARCHAR(50) NOT NULL,
    poblacio VARCHAR(20) NOT NULL DEFAULT 'Terrassa',
    PRIMARY KEY (id_client),
    CONSTRAINT uk_client_telefon UNIQUE (telefon)
)  ENGINE=INNODB;

CREATE TABLE empleat (
    id_empleat SMALLINT(3) AUTO_INCREMENT,
    nom VARCHAR(20) NOT NULL,
    cognoms VARCHAR(40) NOT NULL,
    PRIMARY KEY (id_empleat)
)  ENGINE=INNODB;
    
CREATE TABLE comanda (
    numero SMALLINT AUTO_INCREMENT,
    data_hora TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    domicili_local ENUM('D', 'L') NOT NULL,
    client_id SMALLINT NOT NULL,
    empleat_id SMALLINT(3) NOT NULL,
    PRIMARY KEY (numero),
    CONSTRAINT fk_comanda_client FOREIGN KEY (client_id)
        REFERENCES client (id_client),
    CONSTRAINT fk_comanda_empleat FOREIGN KEY (empleat_id)
        REFERENCES empleat (id_empleat)
)  ENGINE=INNODB , AUTO_INCREMENT=10000;

CREATE TABLE producte (
    id_producte SMALLINT AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    preu DECIMAL(4 , 2 ) NOT NULL,
    PRIMARY KEY (id_producte),
    CONSTRAINT uk_producte_nom UNIQUE (nom),
    CONSTRAINT ck_producte_preu CHECK (preu BETWEEN 0.01 AND 99.99)
)  ENGINE=INNODB;

CREATE TABLE beguda (
    id_producte SMALLINT,
    capacitat SMALLINT(3) NOT NULL,
    alcoholica ENUM('N', 'S') NOT NULL,
    PRIMARY KEY (id_producte),
    CONSTRAINT fk_beguda_producte FOREIGN KEY (id_producte)
        REFERENCES producte (id_producte),
    CONSTRAINT ck_beguda_capacitat CHECK (capacitat BETWEEN 1 AND 150)
)  ENGINE=INNODB;

CREATE TABLE pizza
	(id_producte SMALLINT,
    PRIMARY KEY (id_producte),
    CONSTRAINT fk_pizza_producte FOREIGN KEY (id_producte)
        REFERENCES producte (id_producte)
)  ENGINE=INNODB;

CREATE TABLE postre
	(id_producte SMALLINT,
    PRIMARY KEY (id_producte),
    CONSTRAINT fk_postre_producte FOREIGN KEY (id_producte)
        REFERENCES producte (id_producte)
)  ENGINE=INNODB;

CREATE TABLE ingredient (
    id_ingredient VARCHAR(3),
    nom VARCHAR(20) NOT NULL,
    PRIMARY KEY (id_ingredient),
    CONSTRAINT uk_ingredient_nom UNIQUE (nom)
)  ENGINE=INNODB;

CREATE TABLE pizza_ingredient (
    id_producte SMALLINT,
    id_ingredient VARCHAR(3),
    PRIMARY KEY (id_producte , id_ingredient),
    CONSTRAINT fk_pizza_ingredient_pizza FOREIGN KEY (id_producte)
        REFERENCES pizza (id_producte),
    CONSTRAINT fk_pizza_ingredient_ingredient FOREIGN KEY (id_ingredient)
        REFERENCES ingredient (id_ingredient)
)  ENGINE=INNODB;

CREATE TABLE comanda_producte (
    numero SMALLINT,
    id_producte SMALLINT,
    quantitat SMALLINT(2) NOT NULL,
    PRIMARY KEY (numero , id_producte),
    CONSTRAINT fk_comanda_producte_comanda FOREIGN KEY (numero)
        REFERENCES comanda (numero),
    CONSTRAINT comanda_producte_producte FOREIGN KEY (id_producte)
        REFERENCES producte (id_producte),
    CONSTRAINT ck_comanda_producte_quantitat CHECK (quantitat BETWEEN 1 AND 99)
)  ENGINE=INNODB;

INSERT INTO producte (id_producte, nom, preu) VALUES (1, 'Ampolla Coca-Cola', 1.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (2, 'Ampolla Fanta Llimona', 1.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (3, 'Llauna Nestea', 1.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (4, 'Llauna Cervesa Damm', 1.55);
INSERT INTO producte (id_producte, nom, preu) VALUES (5, 'Llauna Cervesa sense alcohol', 1.55);
INSERT INTO producte (id_producte, nom, preu) VALUES (6, 'Pizza Barbacoa', 19.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (7, 'Pizza Carbonara', 18.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (8, 'Pizza Hawaiana', 16.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (9, 'Pizza 4 estacions', 18.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (10, 'Pizza Ibèrica', 21.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (11, 'Pizza de la casa', 19.95);
INSERT INTO producte (id_producte, nom, preu) VALUES (12, 'Gelat Cornetto Classic', 1.00);
INSERT INTO producte (id_producte, nom, preu) VALUES (13, 'Paquet de trufes de xocolata', 2.25);
INSERT INTO producte (id_producte, nom, preu) VALUES (14, 'Gelat Magnum', 1.95);

INSERT INTO beguda (id_producte, capacitat, alcoholica) VALUES (1, 50,'N');
INSERT INTO beguda (id_producte, capacitat, alcoholica) VALUES (2, 50,'N');
INSERT INTO beguda (id_producte, capacitat, alcoholica) VALUES (3, 33,'N');
INSERT INTO beguda (id_producte, capacitat, alcoholica) VALUES (4, 33,'S');
INSERT INTO beguda (id_producte, capacitat, alcoholica) VALUES (5, 33,'N');

INSERT INTO pizza (id_producte) VALUES (6);
INSERT INTO pizza (id_producte) VALUES (7);
INSERT INTO pizza (id_producte) VALUES (8);
INSERT INTO pizza (id_producte) VALUES (9);
INSERT INTO pizza (id_producte) VALUES (10);
INSERT INTO pizza (id_producte) VALUES (11);

INSERT INTO postre (id_producte) VALUES (12);
INSERT INTO postre (id_producte) VALUES (13);
INSERT INTO postre (id_producte) VALUES (14);

INSERT INTO ingredient (id_ingredient, nom) VALUES ('MOZ', 'Mozzarella');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('STO', 'Salsa de tomàquet');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('BAC', 'Bacon');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('POL', 'Pollastre');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('CAR', 'Carn');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('SBA', 'Salsa barbacoa');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('XAM', 'Xampinyons');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('SCA', 'Salsa carbonara');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('PIN', 'Pinya');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('YOR', 'Pernil york');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('TON', 'Tonyina');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('OLI', 'Olives negres');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('TOM', 'Tomàquet natural');
INSERT INTO ingredient (id_ingredient, nom) VALUES ('IBE', 'Pernil ibèric');

INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (6, 'MOZ');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (6, 'STO');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (6, 'BAC');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (6, 'POL');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (6, 'CAR');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (6, 'SBA');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (7, 'MOZ');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (7, 'STO');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (7, 'XAM');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (7, 'SCA');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (8, 'MOZ');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (8, 'STO');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (8, 'YOR');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (8, 'PIN');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (9, 'MOZ');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (9, 'STO');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (9, 'YOR');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (9, 'TON');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (9, 'OLI');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (9, 'XAM');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (10, 'MOZ');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (10, 'TOM');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (10, 'IBE');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (11, 'MOZ');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (11, 'STO');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (11, 'BAC');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (11, 'YOR');
INSERT INTO pizza_ingredient (id_producte, id_ingredient) VALUES (11, 'CAR');

INSERT INTO client (id_client, nom, telefon, adreca, poblacio) VALUES (001, 'Josep Vila', 937853354, 'C. del Pi 23', 'Terrassa');
INSERT INTO client (id_client, nom, telefon, adreca, poblacio) VALUES (002, 'Carme Garcia', 937883402, 'Plaça Nova 3', 'Terrassa');
INSERT INTO client (id_client, nom, telefon, adreca, poblacio) VALUES (003, 'Enric Miralles', 606989866, 'Carrer Romaní 6', 'Matadepera');
INSERT INTO client (id_client, nom, telefon, adreca, poblacio) VALUES (004, 'Miquel Bover', 937753222, 'Carrer Can Boada 78', 'Terrassa');
INSERT INTO client (id_client, nom, telefon, adreca, poblacio) VALUES (005, 'Marta Ribas', 937862655, 'Carrer Aviació 3', 'Terrassa');
INSERT INTO client (id_client, nom, telefon, adreca, poblacio) VALUES (006, 'Guillem Jam', 937858555, 'Carrer de Dalt 4', 'Terrassa');
INSERT INTO client (id_client, nom, telefon, adreca, poblacio) VALUES (007, 'Júlia Guillén', 626895456, 'C. Robert 8', 'Terrassa');
INSERT INTO client (nom, adreca, poblacio) VALUES ('Josep Vila', 'C. del Pi 23', 'Terrassa');
INSERT INTO client (nom, adreca, poblacio) VALUES ('Carme Garcia', 'Plaça Nova 3', 'Terrassa');
INSERT INTO client (nom, adreca, poblacio) VALUES ('Josep Vila', 'C. del Pi 23', 'Terrassa');

INSERT INTO empleat (id_empleat, nom, cognoms) VALUES (001, 'Jordi', 'Casas');
INSERT INTO empleat (id_empleat, nom, cognoms) VALUES (002, 'Marta', 'Pou');

INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10000, '2017-01-09 20:45:00', 'L', 001, 001);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10001, '2017-01-09 20:51:00', 'D', 002, 001);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10002, '2017-01-09 21:20:00', 'D', 003, 001);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10003, '2017-01-09 21:33:00', 'D', 004, 002);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10004, '2017-01-10 21:00:00', 'D', 005, 001);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10005, '2017-01-10 20:35:00', 'L', 006, 002);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10006, '2017-01-10 21:50:00', 'D', 001, 002);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10007, '2017-01-11 20:32:00', 'D', 002, 001);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10008, '2017-01-11 21:10:00', 'D', 007, 001);
INSERT INTO comanda (numero, data_hora, domicili_local, client_id, empleat_id) VALUES (10009, '2017-01-11 21:24:00', 'D', 001, 002);

INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10000, 6, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10000, 1, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10000, 2, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10000, 12, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10001, 10, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10002, 11, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10002, 3, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10002, 4, 3);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10002, 14, 4);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10003, 7, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10003, 8, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10003, 4, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10003, 5, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10004, 7, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10004, 9, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10004, 1, 6);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10005, 6, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10005, 1, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10005, 12, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10005, 13, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10006, 6, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10006, 10, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10006, 11, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10007, 6, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10007, 1, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10007, 2, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10008, 6, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10008, 4, 2);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10008, 14, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10009, 7, 1);
INSERT INTO comanda_producte (numero, id_producte, quantitat) VALUES (10009, 9, 1);

SELECT DISTINCT nom, adreca, telefon
FROM client
    WHERE poblacio = 'Terrassa' AND telefon != '';
    
SELECT nom, adreca, telefon
FROM client
    WHERE poblacio <> 'Terrassa';

SELECT nom
   FROM client
   WHERE telefon BETWEEN 937850000 AND 937859999;

SELECT nom, cognoms
FROM empleat
    WHERE nom LIKE '%a%' OR cognoms LIKE '%a%';

SELECT numero, data_hora
FROM comanda
	WHERE domicili_local = 'L';
    
SELECT numero
FROM comanda
	WHERE domicili_local = 'D' AND client_id = '001';
    
SELECT nom, preu
FROM producte
	WHERE preu > 17;
    
SELECT nom
FROM ingredient
	WHERE nom LIKE 'Pernil%';
    
SELECT DISTINCT id_producte
FROM pizza_ingredient
	WHERE id_ingredient = 'XAM' OR id_ingredient = 'SCA' OR id_ingredient = 'PIN';
    
SELECT DISTINCT numero
FROM comanda_producte
	WHERE quantitat BETWEEN 3 AND 4;
